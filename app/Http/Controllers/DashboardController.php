<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function show() {
        /** @var User $currentUser */
        $currentUser = Auth::user();

        return Inertia::render('Dashboard', [
            'tasks' => $currentUser->getTasks(),
        ]);
    }
}
