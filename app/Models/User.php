<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPrimaryData() {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
        ];
    }

    public function getTasks() {
        $tasksCollection = $this->tasks()->orderBy('updated_at', 'desc')->get();
        $result = [];
        foreach ($tasksCollection as $task) {
            $result[] = [
                'title' => $task->title,
                'description' => $task->description,
                'user_id' => $task->user_id,
                'created_at' => $task->created_at->format('d.m.Y H:i:s'),
                'updated_at' => $task->updated_at->format('d.m.Y H:i:s'),
            ];
        }
        return $result;
    }

    public function tasks() {
        return $this->hasMany(Task::class);
    }
}
